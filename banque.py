class Comptesimple:

    numero_de_compte=100001 # permet d'attribuer des numeros de comptes aux classes Comptesimple et Comptecourant

    def __init__(self, titulaire, solde = 0,no_compte=0):
        
        self.titulaire = titulaire
        self.__solde = solde
        self.no_compte=Comptesimple.numero_de_compte
        __class__.numero_de_compte +=1

    def crediter(self, titulaire,montant):

        self.__solde += montant

    def debiter(self, titulaire,montant):

        self.__solde -= montant

    @property      # protège le solde en ecriture mais permet la consultation
    def solde(self):
        return self.__solde


class Banque :
    frais = 50

    def __init__(self):
        self.comptes = []

    def open_account(self, depot, titulaire):
        nouveau = Comptesimple(titulaire, depot)
        self.comptes.append(nouveau)

    def total_solde(self):
        total_solde = 0
        for c in self.comptes :
            total_solde += c.solde
        return total_solde
    
    def prelever(self):
        for c in self.comptes :
            c.debiter(self, Banque.frais)

    def open_current_account(self, depot, titulaire):
        nouveau = Comptecourant(titulaire, depot)
        self.comptes.append(nouveau)

    def releve_de_compte(self):
        for compte in self.comptes : 
            print(f'{compte.titulaire} : {compte.solde}')


class Comptecourant(Comptesimple):

    def __init__(self, titulaire, solde,no_compte=0, historique=[]):
        super().__init__(titulaire, solde,no_compte)
        self.historique = historique

    def crediter(self, titulaire,montant):
        super().crediter(titulaire,montant)
        self.historique.append(montant)

    def debiter(self, titulaire,montant):
        super().debiter(titulaire,montant)
        self.historique.append(-montant)

