from banque import *

def test_ouverture_compte():
    C1 = Comptesimple('John', 500)
    assert C1.solde == 500
    assert C1.titulaire == 'John'

def test_crediter_compte():
    C1 = Comptesimple('John', 500)
    C1.crediter('John',200)
    assert C1.solde == 700

def test_debiter_compte():
    C1 = Comptesimple('John', 500)
    C1.debiter('John',200)
    assert C1.solde == 300

def test_debiter_negatif_compte():
    C1 = Comptesimple('John', 500)
    C1.debiter('John',1200)
    assert C1.solde == -700

def test_ouverture_compte_client():
    B1=Banque()
    B1.open_account(500, 'John')
    B1.open_account(200, 'Jack')
    assert len(B1.comptes) == 2

def test_total_solde():
    B1=Banque()
    B1.open_account(500, 'John')
    B1.open_account(200, 'Jack')
    assert B1.total_solde() == 700

def test_prelevement():
    B1=Banque()
    B1.open_account(500, 'John')
    B1.open_account(200, 'Jack')
    assert B1.total_solde() == 700
    B1.prelever()
    assert B1.total_solde() == 600 

def test_historique_compte_courant():
    C1 = Comptecourant('John', 500)
    C1.debiter('John',1200)
    C1.crediter('John',200)
    C1.crediter('John',400)
    print (C1.historique)
    assert C1.solde == -100

def test_ouverture_compte_courant():
    B1=Banque()
    B1.open_current_account(500, 'John')
    B1.open_account(700, 'Jack')
    B1.releve_de_compte()
    assert B1.total_solde() == 1200

def test_numero_de_compte():
    c=Comptesimple("John",22)
    d=Comptesimple("Jack",28)
    e=Comptesimple("Janck",28)
    f=Comptecourant("Peter",500)
    g=Comptecourant("Peteroo",500)

    ref = c.no_compte

    assert c.no_compte == ref
    assert d.no_compte == ref+1
    assert e.no_compte == ref+2
    assert f.no_compte == ref+3
    assert g.no_compte == ref+4

